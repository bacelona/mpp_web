const apiService = {
  "rootUrl": "http://34.235.76.90:8080",
  "path":"/api/v1"
};



const jwt = {
  secret: "qaz123edc8764$1@*7jdkdk569076dsnqouedhdhsdjsh1aurwyr8uiyi9efhsk7485230930dksfdkjzhafjy",
  expiresInMinutes: 60,
  refreshInMinutes: 30
};

const serviceConfig = {
  "getListings" : {
    "path": apiService.path + "/listings"
  },
  "getListingDetails":{
    "path": apiService.path +  "/listing"
  },
  "getNearbyListing":{
    "path": apiService.path +  "/listing/nearbyListing"
  },
  "getSimilarListing":{
    "path": apiService.path +  "/listing/similarListing"
  },
  "searchListing": {
    "path": apiService.path +  "/listing/search"
  },
  "listing" : {
    "path": apiService.path + "/listing"
  },
  "login": {
    "path": apiService.path + "/user/login"
  },
  "register": {
    "path" :apiService.path + "/user/register"
  },
  "logout": {
    "path": apiService.path + "/user/logout"
  },
  "latestListing" :{
    "path": apiService.path + "/listing/latestListing"
  },
  "myListing" :{
    "path": apiService.path + "/listing/mylistings"
  },
  "updateProfile":{
    "path": apiService.path +"/user/update"
  },
  "deleteListing":{
    "path": apiService.path +"/listing"
  }
};

module.exports.apiService = apiService;
module.exports.serviceConfig = serviceConfig;
module.exports.jwt = jwt;
