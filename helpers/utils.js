var numeral = require('numeral');
var _ = require('lodash');

exports.formatNumberListing = function(listings) {
    if(_.isArray(listings)){
      if (listings && listings.length > 0) {
        listings.forEach(function(listing) {
          if (listing.price) {
            if (listing.price) {
              listing.price = numeral(listing.price).format('0,0');
            }
            if (listing.area) {
              listing.area = numeral(listing.area).format('0,0');
            }
          }
        });
      }
    } else {
      if (listings.price) {
        listings.price = numeral(listings.price).format('0,0');
      }
      if (listings.area) {
        listings.area = numeral(listings.area).format('0,0');
      }
    }
  return listings;


};

