const restify = require('restify');
exports.generateUUID = function () {
  var d = Date.now();
  var uuidString = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";
  var uuid = uuidString.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === "x" ? r : r & 3 | 8).toString(16);
  });
  return uuid;
};


exports.createClient = function (serviceUrl) {
  var client;
  if (!serviceUrl) {
    throw new Error("Create Client failed.");
  } else {
    client = restify.createJsonClient({
      url: serviceUrl,
      version: "*",
      connectTimeout: 300000,
      requestTimeout: 300000
    });
    return client;
  }
};

exports.initHeaders = function () {
  let reqHeadersFromUi = {};
  reqHeadersFromUi["REQUEST-TIME"] = reqHeadersFromUi["REQUEST-TIME"] ? reqHeadersFromUi["REQUEST-TIME"] : (reqHeadersFromUi["request-time"] ? reqHeadersFromUi["request-time"] : Date.now());
  reqHeadersFromUi["X-REQUESTED-WITH"] = "XMLHttpRequest";
  reqHeadersFromUi["Content-Type"] = 'application/json';
  reqHeadersFromUi["Accept"] = 'application/json';
  return {headers: reqHeadersFromUi};
};



