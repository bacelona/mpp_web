const logger = require('../helpers/loggerUtil');
const _ = require('lodash');


module.exports = function(config) {
    const apiService = require('../services/apiService')(config);

    return {
        showAccountSetting: function(req,res) {
          /*if (req.headers.authentication) {
            res.render('pages/account-setting',{
              rootUrl: '/'
            });
          } else {
            res.render('errors/401',{
              rootUrl: '/'
            });
          }*/
          res.render('pages/account-setting',{
            rootUrl: '/'
          });
        },
        logout: function(req,res) {
            res.send({result:true});
        },
        register: function(req,res) {
            let body = req.body;
            if (_.isNil(body) || _.isEmpty(body.register_first_name) || _.isEmpty(body.register_last_name)  || _.isEmpty(body.register_email) || _.isEmpty(body.register_password)) {
              res.status(500);
              res.send({
                'message':'Invalid user data',
                'error':'LCC001'
              });
            }else {
              const REGISTER_PATH = config.serviceConfig.register.path;
              const data = {
                firstName: body.register_first_name,
                lastName: body.register_last_name,
                email:body.register_email,
                password: body.register_password
              };


              apiService.post(REGISTER_PATH, data)
                .then((data) => {
                  console.log(data);
                  res.send(data);
                })
                .catch((error) => {
                  logger.error(error.message);
                  res.status(error.statusCode || 500);
                  res.send(error.body);
                });


            }

        },

      login:function(req,res) {
        let body = req.body;
        if (_.isNil(body) || _.isEmpty(body.username) || _.isEmpty(body.password)) {
          res.status(500);
          res.send({
            'message':'Invalid user data',
            'error':'LCC001'
          });
        }else {
          const LOGIN_PATH = config.serviceConfig.login.path;
          const data = {
            username: body.username,
            password: body.password
          };

          apiService.post(LOGIN_PATH, data)
            .then((data) => {
              console.log('error happen');
              res.send(data);
            })
            .catch((error) => {
              console.log(error);
              res.status(error.statusCode || 500);
              res.send(error.body);
            });

        }
      },
      updateProfile:function(req,res) {
          const body = req.body;
          const firstName = body.firstName;
          const lastName = body.lastName;
          const phone = body.phone;
          const password = body.password;
          const confirmPassword = body.confirmPassword;

          if (password !== confirmPassword) {
            res.status(500);
            res.send({
              'error' : {
                'message':'Password does not match'
              }
            })
          }
          let payload = {
            "firstName": firstName,
            "lastName": lastName,
            "password": password,
            "phone": phone
          };

        payload.Authorization = req.headers.authorization;


        apiService.post(config.serviceConfig.updateProfile.path, payload)
          .then((data) => {
            console.log(data);
            res.send(data);
          })
          .catch((error) => {
            console.log(error);
            res.send(error.body);
          });

      }
    }
};
