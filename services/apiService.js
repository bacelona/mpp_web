const restClient = require('../helpers/restClient');


module.exports = function (config) {

  console.log(config.apiService.rootUrl);
  const client = restClient.createClient(config.apiService.rootUrl);

  return {
    getWithToken: function (path,token) {
      return new Promise((resolve, reject) => {
          const options = restClient.initHeaders();
          options.headers.Authorization = token;
          options.path = path;
          console.log(options);
          client.get(options, function (err, request, response, obj) {
            if (err) reject(err);
            else resolve(obj);
          });
        }
      )
    },

    get: function (path) {
      return new Promise((resolve, reject) => {
          const options = restClient.initHeaders();

          options.path = path;
          console.log(options);
          client.get(options, function (err, request, response, obj) {
            if (err) reject(err);
            else resolve(obj);
          });
        }
      )
    },

    post: function (path, payload) {
      return new Promise((resolve, reject) => {
          const options = restClient.initHeaders();
          if (payload.Authorization){
            options.headers.Authorization = payload.Authorization;
          }
          options.path = path;
          client.post(options, payload, function (err, request, response, obj) {
            if (err) {
              console.log('error ...' + JSON.stringify(err));
              reject(err);
            }
            else resolve(obj);
          });
        }
      )
    },

    delete:function(path,payload) {
      return new Promise((resolve, reject) => {
          const options = restClient.initHeaders();
          if (payload.Authorization){
            options.headers.Authorization = payload.Authorization;
          }
          options.path = path;
          console.log(options);
          client.del(options, function (err, request, response, obj) {
            if (err) {
              console.log('error ...' + JSON.stringify(err));
              reject(err);
            }
            else resolve(obj);
          });
        }
      )
    },

    put: function (path, payload) {
      return new Promise((resolve, reject) => {
          const options = restClient.initHeaders();
          options.path = path;
          client.put(options, payload, function (err, request, response, obj) {
            if (err) reject(err);
            else resolve(obj);
          });
        }
      )
    }
  }
};
