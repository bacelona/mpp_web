module.exports = function (app, config) {
  const indexController = require('../controllers/indexController')(config);
  const listingController = require('../controllers/listingController')(config);
  const userController = require('../controllers/userController')(config);
  const authUtil = require('../helpers/authUtil');

  app.get('/', indexController.renderHomePage);
  app.get('/listings', listingController.showListingsPage);
  app.get('/listing/:listingId', listingController.getListingDetail);
  app.get('/listings/nearby', listingController.getNearbyListing);
  app.get('/listing', listingController.showSubmitListing);
  app.get('/photos/:listingId', listingController.showUploadPhoto);
  app.get('/listings/search', listingController.searchListing);
  app.post('/listing', listingController.submitListing);
  //app.post('/photos', listingController.submitListingPhotos);
  app.get('/account/setting', userController.showAccountSetting);
  app.post('/account/setting', userController.updateProfile);
  app.get('/account', listingController.showMyListing);
  app.get('/listings/mylistings', listingController.getMyListings);
  app.get('/logout', userController.logout);
  app.get('/account/search', listingController.showSavedSearch);
  app.get('/account/getSavedSearch', listingController.getSavedSearch);
  app.post('/register', userController.register);
  app.post('/login', userController.login);
  app.post('/photo/upload',listingController.uploadPhoto);
  app.post('/profile/update',userController.updateProfile);
  app.post('/listing/delete',listingController.deleteListing);

};
