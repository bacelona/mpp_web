const logger = require('../helpers/loggerUtil');
var numeral = require('numeral');
const async = require('async');
const util = require('../helpers/utils');

module.exports = function (config) {
  const apiService = require('../services/apiService')(config);

  var proxy = require("http-proxy").createProxyServer({
    target: config.apiService.rootUrl + config.apiService.path
  });
  proxy.on("error", function (err, req, res) {
    logger.info("error", req);
    res.json({
      code: 500,
      error: err
    });
  });

  return {
    showListingsPage: function (req, res) {
      const listingPath = config.serviceConfig.searchListing.path;

      var payload = {
        "latitude" : req.query.latitude,
        "longitude": req.query.longitude,
        "radius": req.query.radius || 50,
        "page":req.query.page || 0,
        "size":req.query.size || 5
      };

      console.log(payload);

      apiService.post(listingPath,payload).then((result) => {
        console.log('---------');
        console.log(result);
        res.render("pages/listings", {
          rootUrl: '/',
          data:result
        });
      }).catch((error) => {
        console.log('error');
        console.log(error.body);
        res.render("pages/listings", {
          rootUrl: '/'
        });
      });

    },
    getListingDetail: function (req, res) {
      logger.info("Printing inside getListingDetail: " + JSON.stringify(req.body));
      const listingId = req.params.listingId;
      const listingDetailsPath = config.serviceConfig.getListingDetails.path +'/' + listingId;
      const nearbyListingPath =  config.serviceConfig.getNearbyListing.path +'?listingId=' + listingId;
      const similarListingPath = config.serviceConfig.getSimilarListing.path +'?listingId=' + listingId;
      console.log(listingDetailsPath);

      async.parallel({
        task1: function(callback) {
              apiService.get(listingDetailsPath).then((result) => {
                //const listings = util.formatNumberListing(result);
                callback(null,result);
              }).catch((error) => {
                callback(error);
              });
        },
        task2: function(callback) {
              apiService.get(nearbyListingPath).then((result) => {
                callback(null,result);
              }).catch((error) => {
                callback(error);
              })
        },
        task3: function(callback) {
              apiService.get(similarListingPath).then((result) => {
                callback(null,result);
              }).catch((error) => {
                callback(error);
              })
        }
      }, function(err, results) {
        console.log(results);
        // results now equals to: { task1: 1, task2: 2 }
        res.render("pages/listing-detail", {
          rootUrl: '/',
          listingId:listingId,
          listing: util.formatNumberListing(results.task1),
          nearbyListings: results.task2 && results.task2.listings ? util.formatNumberListing(results.task2.listings) : [],
          similarListings: results.task3 && results.task3.listings ? util.formatNumberListing(results.task3.listings) : [],
        });
      });

    },
    getNearbyListing: function (req, res) {

      const nearbyListingPath = config.serviceConfig.getNearbyListing.path +'?listingId=' + req.params.listingId;
      console.log(nearbyListingPath);

      apiService.get(nearbyListingPath).then((result) => {
        res.render("partials/vertical-listing", {
          rootUrl: '/',
          listings:result
        });
      }).catch((error) => {
        res.send(error.body);
      });
    },
    searchListing: function (req, res) {
      const listingPath = config.serviceConfig.searchListing.path;


      var payload = {
        "latitude" : req.query.latitude,
        "longitude": req.query.longitude,
        "radius": req.query.radius || 50,
        "page":req.query.page || 0,
        "size":100,
        "area": req.query.area,
        "numBath": req.query.numBath,
        "numBed": req.query.numBed,
        "price":req.query.price,
        "listType":req.query.listType
      };

      console.log(payload);

      apiService.post(listingPath,payload).then((result) => {


        res.render("partials/listings", {
          rootUrl: '/',
          data:result
        });
      }).catch((error) => {
        console.log('error');
        console.log(error.body);
        res.send(error.body);
      });


    },

    showSubmitListing: function (req, res) {
      logger.info("Printing inside getListingDetail: " + JSON.stringify(req.body));
      res.render('pages/new-listing', {
        rootUrl: '/'
      });
    },
    showUploadPhoto: function (req, res) {
      logger.info("Printing inside getListingDetail: " + JSON.stringify(req.body));
      res.render('pages/photo-listing', {
        rootUrl: '/',
        listingId:req.params.listingId
      });
    },
    submitListing: function (req, res) {
      console.log('creating listing ...' + JSON.stringify(req.body));
      let payload = req.body;
      payload.Authorization = req.headers.authorization;
      apiService.post(config.serviceConfig.listing.path, payload)
        .then((data) => {
          console.log(data);
          res.send(data);
        })
        .catch((error) => {
          console.log(error.message);
          res.status(error.statusCode || 500);
          res.send(error.body);
        });
    },

    showMyListing: function (req, res) {
      res.render('pages/my-listings', {
        rootUrl: '/'
      });

      /*apiService.get(config.serviceConfig.myListing.path,req.headers.authorization)
        .then((data) => {
          console.log(data);
          res.render('partials/my-listings', {
            rootUrl: '/'
          });
        })
        .catch((error) => {
          console.log(error);
          res.render('partials/my-listings', {
            rootUrl: '/'
          });
        });*/


    },
    showSavedSearch: function (req, res) {
      res.render('pages/saved-search', {
        rootUrl: '/'
      });
    },
    getSavedSearch: function (req, res) {
      res.send({result: 'abcd'});
    },
    uploadPhoto:function(req,res) {
      req.url = "/listing/uploadImage";
      proxy.web(req, res);
    },
    deleteListing : function(req,res) {
      const listingId = req.body.id;
      const payload ={
        Authorization : req.headers.authorization
      };
      apiService.delete(config.serviceConfig.deleteListing.path +"/" + listingId,payload)
        .then((result) =>{
          res.send(result);
        })
        .catch((error) => {
          res.statusCode = 500;
          res.send(error.body);
        })
    },

    getMyListings: function(req,res) {

      apiService.getWithToken(config.serviceConfig.myListing.path,req.headers.authorization)
        .then((listings) => {
          const activeListings = [];
          const inactiveListings = [];
          if (listings && listings.length > 0) {
            listings = util.formatNumberListing(listings);
              listings.forEach(function (listing) {
                //console.log(listing);
                //  if (listing.status == 'pending'){
                    activeListings.push(listing)
                //  } else {
                //    inactiveListings.push(listing);
                 // }
              })
          }
          res.render('partials/my-listings', {
            rootUrl: '/',
            activeListings:activeListings,
            inactiveListings:inactiveListings
          });
        })
        .catch((error) => {
          console.log(error);
          const statusCode = error.statusCode;
          res.render('partials/' + statusCode, {
            rootUrl: '/'
          });
        });
    }
  }
};
