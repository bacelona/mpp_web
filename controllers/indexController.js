const logger = require('../helpers/loggerUtil');
const util = require('../helpers/utils');

module.exports = function(config) {
    const apiService = require('../services/apiService')(config);
    return {
        renderHomePage: function(req,res) {

          const latestListingPath = config.serviceConfig.latestListing.path +'?limit=' + (req.params.limit ? req.params.limit : 6);

          apiService.get(latestListingPath).then((result) => {

            res.render("pages/home",{
              rootUrl: '/',
              listings:util.formatNumberListing(result)
            });
          }).catch((error) => {
            res.render("pages/home",{
              rootUrl: '/'
            });
          });

        }
    }
};


