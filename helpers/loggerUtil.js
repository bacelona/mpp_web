const winston = require('winston');
const stringify = require("json-stringify-safe");
const moment = require("moment-timezone");

function formatter(args) {
    const date = moment.tz().format("YYYY-MM-D HH:mm:ss.SSS z");
    let msg = "";
    if (args && (args.level === "info" || args.level === "access") && args.message.indexOf("body received") < 0) {
        if (args.level === "info") {
            if (args.message.toLowerCase().indexOf("request sent") > -1) {
                if (args.meta.client_req && args.meta.client_req.headers) {
                    return date + "|OUT|" + args.meta.client_req.headers["x-auth-user-name"] + "|" + args.meta.client_req.headers["x-wst-pa"] + "|" + args.meta.client_req.headers["correlation-id"] + "|" + args.meta.client_req.address + args.meta.client_req.url + "|" + args.meta.client_req.headers["REQUEST-TIME"] + "||ms|";
                }
            } else if (args.message.toLowerCase().indexOf("response received") > -1) {
                if (args.meta.client_res && args.meta.client_res.headers) {
                    var rquestTime = args.meta.client_res.headers["request-time"] || "";
                    var responseTime = "";
                    if (rquestTime.length > 0) {
                        responseTime = moment().valueOf() - rquestTime;
                    }
                    return date + "|OUT-IN|||" + args.meta.client_res.headers["correlation-id"] + "||" + args.meta.client_res.headers["request-time"] + "|" + responseTime + "|ms|" + args.meta.client_res.statusCode;
                }
            }
        } else if (args.level === "access") {
            msg = date + "|" + args.message;
        }
    }
    return msg;
}

function formatterRestify(args) {
    const date = moment.tz().format("YYYY-MM-D HH:mm:ss.SSS z");
    let msg = "";
    if (args && args.level === "info" && args.message.indexOf("body received") < 0) {
        if (args.meta.client_req) {
            msg = args.level.toUpperCase() + " | " + date + " | [] Class: Method: Line: - " + args.meta.client_req.headers["correlation-id"] + " - " + stringify(args);
        } else if (args.meta.client_res) {
            msg = args.level.toUpperCase() + " | " + date + " | [] Class: Method: Line: - " + args.meta.client_res.headers["correlation-id"] + " - " + stringify(args);
        } else {
            msg = args.level.toUpperCase() + " | " + date + " | [] Class: Method: Line: - - " + stringify(args);
        }
    }
    return msg;
}

function formatterError(args) {
    var date = moment.tz().format("YYYY-MM-D HH:mm:ss.SSS z");
    let msg = "";
    if (args && args.level === "error") {
        if (args.meta.client_req) {
            msg = args.level.toUpperCase() + " | " + date + " | [] Class: Method: Line: - " + args.meta.client_req.headers["correlation-id"] + " - " + stringify(args);
        } else if (args.meta.client_res) {
            msg = args.level.toUpperCase() + " | " + date + " | [] Class: Method: Line: - " + args.meta.client_res.headers["correlation-id"] + " - " + stringify(args);
        } else {
            msg = args.level.toUpperCase() + " | " + date + " | [] Class: Method: Line: - - " + stringify(args);
        }
    }
    return msg;
}

var transports = [];
if (!process.env.CONSOLEONLY) {
    if (process.env.NODE_ENV !== "local") {
        transports = [
            new winston.transports.File({
                name: "info-file",
                filename: "logs/application.log",
                level: "info",
                json: false,
                timestamp: true,
                formatter: formatterRestify
            }),
            new winston.transports.File({
                name: "error-file",
                filename: "logs/error.log",
                level: "error",
                timestamp: true,
                json: false,
                formatter: formatterError
            }),
            new winston.transports.File({
                name: "access-file",
                filename: "logs/access.log",
                level: "access",
                json: false,
                timestamp: true,
                formatter: formatter
            })
        ];
    } else {
        transports = [new winston.transports.Console({timestamp: true})];
    }
}
const myCustomLevels = {
    levels: {
        error: 0,
        warn: 1,
        info: 2,
        access: 3
    }
};
const logger = new winston.createLogger({
    transports: transports,
    levels: myCustomLevels.levels
});

module.exports = logger;
