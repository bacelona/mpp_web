const apiService = {
  "rootUrl": "http://34.235.76.90:8080",
  "path": "/api/v1"
};

const jwt = {
  secret: "qaz123edc8764$1@*7jdkdk569076dsnqouedhdhsdjsh1aurwyr8uiyi9efhsk7485230930dksfdkjzhafjy",
  expiresInMinutes: 60,
  refreshInMinutes: 30
};

const serviceConfig = {
  "listing": {
    "path": apiService.path + "/listing"
  },
  "login": {
    "path": apiService.path + "/login"
  },
  "register": {
    "path": apiService.path + "/register"
  },
  "logout": {
    "path": apiService.path + "/logout"
  }
};

module.exports.apiService = apiService;
module.exports.serviceConfig = serviceConfig;
module.exports.jwt = jwt;
