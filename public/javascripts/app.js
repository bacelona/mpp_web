let USER_KEY = "user";


function isUserLogin() {
  const data = localStorage.getItem(USER_KEY);
  const user = JSON.parse(data);
  if (user && user.accessToken && user.email && user.id) {
    document.getElementById('user_full_name').innerText = user.firstName +' ' + user.lastName;
    return true;
  } else {
    return false;
  }
}

function getAccessToken() {
  try {
    const data = localStorage.getItem(USER_KEY);
    const user = JSON.parse(data);
    if (user && user.accessToken && user.email && user.id) {
      return user.accessToken;
    } else {
      return "";
    }
  }catch (e) {

  }
}
function logout() {
  localStorage.setItem(USER_KEY,null);
  localStorage.clear();
}
