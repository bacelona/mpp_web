var _ = require('lodash'),
    fs = require('fs'),
    path = require('path');

if(process.env.NODE_ENV){
    process.env.NODE_ENV = ~fs.readdirSync(path.join('./config', 'env')).map(function(file) {
        return file.slice(0, -3);
    }).indexOf(process.env.NODE_ENV)>-1 ? process.env.NODE_ENV : 'dev';
}
else{
    process.env.NODE_ENV = 'dev';
}

var CONFIG_ENV = process.env.CONFIG_ENV || process.env.NODE_ENV;
// Extend the base configuration in all.js with environment specific configuration
module.exports = _.extend(
    //require('./env/all'),
    require('./env/' + CONFIG_ENV + '/config') || {}
);
